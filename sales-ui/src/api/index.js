
// const getDatefromPeriod = (period) => {

//   let today = new Date();
//   let timestamp = ""
//   if (period == "Today") {
//     timestamp = new Date(today.getFullYear(), today.getMonth(), today.getDate()).getTime();
//   }
//   switch (period) {
//     case 'Last Week':

//       break;
//     default:
//       timestamp = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()).getTime();

//   }
//   return timestamp
// }
const baseUrl = process.env.VUE_APP_API_URL;
export const getSalesDetails = (period, salesRep) => {
  let url = `${baseUrl}api/sales-details?period=${period}&salesRep=${salesRep}`
  // eslint-disable-next-line no-console
  console.log(url)

  return fetch(url, {
    mode: "cors", // no-cors, cors, *same-origin
    // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    // credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      "Content-Type": "application/json"
      // 'Content-Type': 'application/x-www-form-urlencoded',
    }
    // redirect: 'follow', // manual, *follow, error
    // referrer: "no-referrer" // no-referrer, *client
    // body: JSON.stringify(data) // body data type must match "Content-Type" header
  })

};

export const getSaleReps = (salesRep) => {
  let url = `${baseUrl}api/sales-reps?salesRep=${(salesRep ? salesRep : "")}`;
  return fetch(url, {
    headers: {
      "Content-Type": "application/json"
      // 'Content-Type': 'application/x-www-form-urlencoded',
    }
  })

};

export const getSalesFunnelStats = (period, salesRep) => {
  let url = `${baseUrl}api/sales-funnel-stats?period=${period}&salesRep=${salesRep}`;

  return fetch(url, {
    headers: {
      "Content-Type": "application/json"
      // 'Content-Type': 'application/x-www-form-urlencoded',
    }
  })

};