const uuidv4 = require("uuid/v4");
// Load the AWS SDK for Node.js
var AWS = require("aws-sdk");
// Set the region
AWS.config.update({ region: "us-east-2" });

// Create the DynamoDB service object
var ddb = new AWS.DynamoDB({ apiVersion: "2012-08-10" });

var params = {
  RequestItems: {
    SALES_REP: [
      {
        PutRequest: {
          Item: {
            id: { N: "1" },
            name: { S: "Jane Doe" }
          }
        }
      },
      {
        PutRequest: {
          Item: {
            id: { N: "2" },
            name: { S: "Tom Roberts" }
          }
        }
      },
      {
        PutRequest: {
          Item: {
            id: { N: "3" },
            name: { S: "Ethan Hunt" }
          }
        }
      },
      {
        PutRequest: {
          Item: {
            id: { N: "4" },
            name: { S: "John Doe  " }
          }
        }
      }
    ]
  }
};

ddb.batchWriteItem(params, function (err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data);
  }
});
