// Load the AWS SDK for Node.js
var AWS = require("aws-sdk");
// Set the region
AWS.config.update({ region: "us-east-2" });

// Create the DynamoDB service object
var ddb = new AWS.DynamoDB({ apiVersion: "2012-08-10" });

var params = {
  AttributeDefinitions: [
    { AttributeName: "ID", AttributeType: "S" },
    { AttributeName: "EVENT_DATE", AttributeType: "N" }
    // { AttributeName: "SALES_REP_NAME", AttributeType: "S" },
  ],
  KeySchema: [
    { AttributeName: "ID", KeyType: "HASH" },
    { AttributeName: "EVENT_DATE", KeyType: "RANGE" }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 1
  },
  TableName: "SALES_EVENT",
  StreamSpecification: {
    StreamEnabled: false
  }
};

var rep_params = {
  AttributeDefinitions: [
    { AttributeName: "id", AttributeType: "N" },
    { AttributeName: "name", AttributeType: "S" }
    // { AttributeName: "SALES_REP_NAME", AttributeType: "S" },
  ],
  KeySchema: [
    { AttributeName: "id", KeyType: "HASH" },
    { AttributeName: "name", KeyType: "RANGE" }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
  },
  TableName: "SALES_REP",
  StreamSpecification: {
    StreamEnabled: false
  }
};

// Call DynamoDB to create the table
ddb.createTable(params, function (err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Table Created", data);
  }
});

// Call DynamoDB to create the table
ddb.createTable(rep_params, function (err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Table Created", data);
  }
});
