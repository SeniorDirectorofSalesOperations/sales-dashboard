const { getDateRange, initialStats, engagementStatusList } = require('../domain')


module.exports = (router, docClient) => {
    router.get("/sales-funnel-stats", async (req, res, next) => {
        // Access the provided 'page' and 'limt' query parameters
        console.log(req.query);
        let period = req.query.period;
        let salesRep = req.query.salesRep;
        let today = new Date();
        let { from, to } = getDateRange(period, today)


        const params = {
            TableName: "SALES_EVENT",
            IndexName: "StatusEventIndex",
            KeyConditionExpression: "engagementStatus = :pk and EVENT_DATE between :sk1 and :sk2",
            ExpressionAttributeValues: {
                ":sk1": from,
                ":sk2": to,
            },
            // "ProjectionExpression": "UserId, TopScore",
            ScanIndexForward: false
        }
        if (salesRep != 0) {
            params["filterExpression"] = `SALES_REP_ID = ${salesRep}`
        }

        console.log(params)
        let result = {
            // sales_events: [],
            funnel_stats: initialStats()
        }
        engagementStatusList.forEach(status => {
            params.ExpressionAttributeValues[":pk"] = status
            docClient.query(params, function scanUntilDone(err, data) {
                if (err) {
                    console.error(err);
                    res.json({
                        success: false,
                        message: "Error: Server error"
                    });
                } else {

                    if (data.LastEvaluatedKey) {
                        console.log(`LastEvaluatedKey: ${data.LastEvaluatedKey}`)
                        params.ExclusiveStartKey = data.LastEvaluatedKey;
                        updateResults(result, data, status)
                        dynamodb.scan(params, scanUntilDone);
                    } else {
                        // all results processed. done
                        updateResults(result, data, status)

                        res.json({
                            success: true,
                            message: "Loaded Sales Funnel Stats",
                            ...result
                        });
                        console.log(result.sales_events.length)

                    }
                }
            });
        })
    })
}

const updateResults = (result, data, status) => {
    if (Items.length > 0) {
        result.funnel_stats[status] += data.Count;
        console.log(result.funnel_stats)
    }

}

const getEngagementStatusCounts = (items, funnel_stats) => {

    return items.reduce((stats, item) => {

        // console.log(stats, item)
        if (item.engagementStatus in stats)
            stats[item.engagementStatus] += 1
        return stats
    }, funnel_stats)
}

