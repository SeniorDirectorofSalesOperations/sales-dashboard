const express = require("express");
const cors = require("cors");
const app = express();
const port = 3000;
app.use(cors({ methods: "GET" }));
// Load the SDK for JavaScript
var AWS = require("aws-sdk");
// Set the region
AWS.config.update({ region: "us-east-2" });
var docClient;
try {
  docClient = new AWS.DynamoDB.DocumentClient();
} catch (error) {
  console.error(error)
  return;
}

app.get("/", (req, res) => res.send("Hello World!"));
const router = express.Router()

require('./api/sales-reps')(router, docClient)
require('./api/sales-details')(router, docClient)
require('./api/sales-funnel-stats')(router, docClient)



app.use("/api", router)

module.exports = app;
